//
//  mmViewController.m
//  TodayNews
//
//  Created by developer on 2016/12/5.
//  Copyright © 2016年 developer. All rights reserved.
//

#import "mmViewController.h"
#import "OneViewController.h"
#import "TwoViewController.h"
#import "ThreeViewController.h"
#import "FourViewController.h"
#import "FiveViewController.h"
#import "SixViewController.h"
#import "SevenViewController.h"

@interface mmViewController ()

@end

@implementation mmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupAllChildViewController];
}

- (void)setupAllChildViewController {
    OneViewController *one = [[OneViewController alloc]init];
    one.title = @"One";
    [self addChildViewController:one];
    
    TwoViewController *two = [[TwoViewController alloc]init];
    two.title = @"Two";
    [self addChildViewController:two];
    
    ThreeViewController *three = [[ThreeViewController alloc]init];
    three.title = @"Three";
    [self addChildViewController:three];
    
    FourViewController *four = [[FourViewController alloc]init];
    four.title = @"Four";
    [self addChildViewController:four];
    
    FiveViewController *five = [[FiveViewController alloc]init];
    five.title = @"Five";
    [self addChildViewController:five];
    
    SixViewController *six = [[SixViewController alloc]init];
    six.title = @"Six";
    [self addChildViewController:six];
    
    SevenViewController *seven = [[SevenViewController alloc]init];
    seven.title = @"Seven";
    [self addChildViewController:seven];
}
@end
