//
//  ViewController.m
//  TodayNews
//
//  Created by developer on 2016/12/5.
//  Copyright © 2016年 developer. All rights reserved.
//
#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height

#import "ViewController.h"

@interface ViewController ()<UIScrollViewDelegate>
@property (nonatomic, weak) UIScrollView *titleScrollView;
@property (nonatomic, weak) UIScrollView *contentScrollView;
@property (nonatomic, weak) UIButton *selectButton;
@property (nonatomic, strong) NSMutableArray *titleButtons;

@property (nonatomic, assign) BOOL isInitialize;
@end

@implementation ViewController
- (NSMutableArray *)titleButtons
{
    if (_titleButtons == nil) {
        _titleButtons = [NSMutableArray array];
    }
    return _titleButtons;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (_isInitialize == NO) {
        // 4.设置所有标题
        [self setupAllTitle];
        
        _isInitialize = YES;
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"网易新闻";
    
    // 1.添加标题滚动视图
    [self setupTitleScrollView];
    
    // 2.添加内容滚动视图
    [self setupContentScrollView];
    
    // 3.添加所有子控制器
    

    // 4.设置所有标题
//    [self setupAllTitle];
    
    // iOS7以后,导航控制器中scollView顶部会添加64的额外滚动区域
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)setupOneViewController:(NSInteger)i{
    UIViewController *vc = self.childViewControllers[i];
    if (vc.view.superview) {
        return;
    }
    vc.view.frame = CGRectMake(i * ScreenW, 0, ScreenW, self.contentScrollView.bounds.size.height);
    [self.contentScrollView addSubview:vc.view];
    
}
- (void)setupTitleCenter:(UIButton *)btn{
    // 本质:修改titleScrollView偏移量
    CGFloat offsetX = btn.center.x - ScreenW / 2;

    if (offsetX < 0) {
        return;
    }
    CGFloat maxOffsetX = self.titleScrollView.contentSize.width - ScreenW;
    if (offsetX > maxOffsetX) {
        offsetX = maxOffsetX;
    }
    [self.titleScrollView setContentOffset: CGPointMake(offsetX, 0) animated:YES];
}
- (void)selButton:(UIButton *)btn{
    self.selectButton.transform = CGAffineTransformIdentity;
    [self.selectButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    btn.transform = CGAffineTransformMakeScale(1.2, 1.2);
    //标题居中
    [self setupTitleCenter:btn];
    self.selectButton = btn;
    
}
- (void)titleClick:(UIButton *)btn{
    NSInteger i = btn.tag;
    [self selButton:btn];
    
    // 2.把对应子控制器的view添加上去
    [self setupOneViewController:i];
    self.contentScrollView.contentOffset = CGPointMake(i * ScreenW, 0);
}
- (void)setupAllTitle {
    NSUInteger count = self.childViewControllers.count;
    CGFloat btnW = ScreenW / 5;
    CGFloat btnH = 44;
    CGFloat btnX = 0;
    for (NSInteger i = 0; i < count; i++) {
        UIViewController *vc = self.childViewControllers[i];
        UIButton *btn = [[UIButton alloc]init];
        btn.tag = i;
        btnX = btnW * i;
        btn.frame = CGRectMake(btnX, 0, btnW, btnH);
        [btn setTitle:vc.title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.titleScrollView addSubview:btn];
        
        // 监听按钮点击
        [btn addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i == 0) {
            [self titleClick:btn];
        }
        
        [self.titleButtons addObject:btn];
    }
    
    self.titleScrollView.contentSize = CGSizeMake(btnW * count, 44);
    self.titleScrollView.showsHorizontalScrollIndicator = false;
    
    
    // 设置内容的滚动范围
    self.contentScrollView.contentSize = CGSizeMake(count * ScreenW, 0);
    self.contentScrollView.showsHorizontalScrollIndicator = false;
    self.contentScrollView.pagingEnabled = YES;
    self.contentScrollView.bounces = YES;
    self.contentScrollView.delegate = self;
}

#pragma mark - 添加内容滚动视图
- (void)setupContentScrollView {
    // 创建contentScrollView
    UIScrollView *contentScrollView = [[UIScrollView alloc] init];
    CGFloat y = CGRectGetMaxY(self.titleScrollView.frame);
    contentScrollView.frame = CGRectMake(0, y, ScreenW, ScreenH - y);
    [self.view addSubview:contentScrollView];
    _contentScrollView = contentScrollView;
}
#pragma mark - 添加标题滚动视图
- (void)setupTitleScrollView {
    UIScrollView *titleScrollView = [[UIScrollView alloc] init];
    CGFloat y = self.navigationController.navigationBarHidden? 20 : 64;
    titleScrollView.frame = CGRectMake(0, y, ScreenW, 44);
    [self.view addSubview:titleScrollView];
//    titleScrollView.backgroundColor = [UIColor orangeColor];
    _titleScrollView = titleScrollView;
}
#pragma mark - UIScrollViewDelegate
//滚动结束
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // 获取当前角标
    NSInteger i = scrollView.contentOffset.x / ScreenW;
    
    // 获取标题按钮
    UIButton *titleButton = self.titleButtons[i];
    
    // 1.选中标题
    [self selButton:titleButton];
    
    // 2.把对应子控制器的view添加上去
    [self setupOneViewController:i];
}
// 只要一滚动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSInteger leftI = scrollView.contentOffset.x / ScreenW;
    NSInteger rightI = leftI + 1;
    
    // 获取左边的按钮
    UIButton *leftBtn = self.titleButtons[leftI];
    NSInteger count = self.titleButtons.count;
    
    // 获取右边的按钮
    UIButton *rightBtn;
    if (rightI < count) {
        rightBtn = self.titleButtons[rightI];
    }
    
    CGFloat scaleR =  scrollView.contentOffset.x / ScreenW;
    scaleR -= leftI;
    CGFloat scaleL = 1 - scaleR;
    
    // 缩放按钮
    leftBtn.transform = CGAffineTransformMakeScale(scaleL * 0.3 + 1, scaleL * 0.3 + 1);
    rightBtn.transform = CGAffineTransformMakeScale(scaleR * 0.3 + 1, scaleR * 0.3 + 1);
    
    // 颜色渐变
    UIColor *rightColor = [UIColor colorWithRed:scaleR green:0 blue:0 alpha:1];
    UIColor *leftColor = [UIColor colorWithRed:scaleL green:0 blue:0 alpha:1];
    [rightBtn setTitleColor:rightColor forState:UIControlStateNormal];
    [leftBtn setTitleColor:leftColor forState:UIControlStateNormal];
}
@end
